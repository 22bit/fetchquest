# Groceries

* [ ] Almonds
* [ ] Beef Jerky
* [ ] Diet Soda
* [ ] Hall's Sugar-Free Citrus Blend Cough Drops
* [ ] Hot Dogs
* [ ] London Broil
* [ ] Parmesan Crisps
* [ ] Pepto
* [ ] Sunflower Seeds
* [ ] Vodka
* [x] Antacids
* [x] Atkins Shakes
* [x] Bacon
* [x] Butter
* [x] Butter Lettuce
* [x] Cauliflower Faux-tatoes
* [x] Chicken
* [x] Chicken Wings
* [x] Cider Vinegar
* [x] Cream Cheese
* [x] Creamer
* [x] Dishwasher Detergent
* [x] Dry Italian Dressing Mix Packet
* [x] Eggs
* [x] Frozen Broccoli
* [x] Frozen Hamburgers
* [x] Frozen Spinach
* [x] G. Hughes Smokehouse Sugar-Free Asian Miso Dressing
* [x] G. Hughes Smokehouse Sugar-Free BBQ Sauce
* [x] Ground Meat
* [x] Heavy Cream
* [x] Hot Sauce
* [x] Ice
* [x] Jello
* [x] Mayo
* [x] Mezetta-brand Giardiniera Mix (12oz Jar)
* [x] Mint Tea
* [x] Mt. Olive-brand Pepperoccini (12oz Jar)
* [x] Mustard
* [x] Napkins
* [x] Painkillers
* [x] Plain Pork Rinds
* [x] Pork Butt
* [x] Pork Chops
* [x] Pork Rinds
* [x] Protein Puffs
* [x] Q-Tips
* [x] Riced Cauliflower
* [x] Salad
* [x] Salad Dressing
* [x] Salsa
* [x] Seasoned Salt
* [x] Shredded Cheese
* [x] Sliced Almonds
* [x] Sliced Cheese
* [x] Sliced Mushrooms
* [x] Sliced Pepperoni
* [x] Smoked Sausage
* [x] Sour Cream
* [x] Soy Sauce
* [x] Steak
* [x] String Cheese
* [x] Toilet Paper
* [x] Tuna
* [x] Turkey Breast
* [x] Wet Wipes
* [x] Worchestershire Sauce
* [x] Yogurt
* [x] Ziploc Bags (Gallon)
